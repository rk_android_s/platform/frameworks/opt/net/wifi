package librkwifi_ctrl

import (
        "android/soong/android"
        "android/soong/cc"
        "fmt"
        "strings"
)

func init() {
    fmt.Println("librkwifi-ctrl want to conditional Compile")
    android.RegisterModuleType("cc_librkwifi_ctrl", DefaultsFactory)
}

func DefaultsFactory() (android.Module) {
    module := cc.DefaultsFactory()
    android.AddLoadHook(module, Defaults)
    return module
}

func Defaults(ctx android.LoadHookContext) {
	type props struct {
		Cflags []string
	}

	p := &props{}
	p.Cflags = getCflags(ctx)

	ctx.AppendProperties(p)
}

func getCflags(ctx android.BaseContext) ([]string) {
    var cppflags []string

    if (strings.EqualFold(ctx.AConfig().Getenv("BOARD_CONNECTIVITY_VENDOR"),"Infineon") ) {
		cppflags = append(cppflags, "-DBOARD_CONNECTIVITY_VENDOR_INFINEON")
    }

    return cppflags
}
